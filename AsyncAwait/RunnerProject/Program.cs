﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Async_await_Task;

namespace RunnerProject
{
    public class Program
    {
        public static  void Main(string[] args)
        {
            MainAsync().Wait();
        }
        /// <summary>
        /// Runner method
        /// </summary>
        /// <returns></returns>
        public static async Task MainAsync()
        {
            // path to folder
            Console.WriteLine("Enter path:");
            string path = Console.ReadLine();
            //url to be parsed
            Console.WriteLine("Enter url:");
            string url = Console.ReadLine();
            // depth of parsing
            Console.WriteLine("Enter amount of parsing levels:");
            int level = int.Parse(Console.ReadLine());

            var saver = new HtmlParser(url, path, level);
            await saver.StartSavingAsync();
        }
    }
}
