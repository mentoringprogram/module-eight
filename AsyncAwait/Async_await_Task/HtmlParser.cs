﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AngleSharp.Dom;
using AngleSharp.Dom.Html;

namespace Async_await_Task
{
    public class HtmlParser
    {
        // files to be saved
        private readonly string[] fileExtensions = { ".jpg", ".png", ".gif", ".pdf" };
        private string url;
        // initial path to folder
        private readonly string initPath;

        private readonly int loadLevel;
        // regular expressions for skip https/http urls parts
        private readonly Regex regexForSavedAddress = new Regex(@"^https://.+\..+");
        private readonly Regex regexForUnsavedAddress = new Regex(@"^http://.+\..+");
        // regexp to get name of url after http/https
        private readonly Regex regexForAddress = new Regex(@"^.+\..+");

        public HtmlParser(string url, string path, int loadLevel)
        {
            this.url = string.IsNullOrEmpty(url) ? throw new ArgumentNullException() : url;
            initPath = string.IsNullOrEmpty(path) || !Directory.Exists(path) ? throw new ArgumentNullException() : path;
            this.loadLevel = loadLevel;
        }

        public async Task StartSavingAsync()
        {
            IEnumerable<char> urlPath = "";

            var matchesForSavedAddress = regexForSavedAddress.Matches(url);
            // skip https in url
            if (matchesForSavedAddress.Count > 0)
            {
                //take name
                urlPath = url.Skip(8);
            }

            var matchesForUnsavedAddress = regexForUnsavedAddress.Matches(url);
            // skip http in url
            if (matchesForUnsavedAddress.Count > 0 && string.IsNullOrWhiteSpace(urlPath.ToString()))
            {
                // take name
                urlPath = url.Skip(7);
            }

            var matchesForAddress = regexForAddress.Matches(url);

            if (matchesForAddress.Count > 0 && string.IsNullOrWhiteSpace(urlPath.ToString()))
            {
                // take name after domain: tut.by/newsName
                // we take newsName
                urlPath = url;
            }

            var builder = new StringBuilder();

            foreach (var a in urlPath)
            {
                builder.Append(a);
            }

            // create folder
            var path = CreateFolder(initPath, builder.ToString());
            await StartSavingAsync(loadLevel, null, path);
        }

        private string CreateFolder(string currentPath, string folderName)
        {
            var path = currentPath + @"\" + folderName;

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }

        private async Task<string> DownloadHtmlAsync(string url)
        {
            //download html with HttpClient
            using (var req = new HttpClient())
            {
                using (var response = await req.GetAsync(url))
                {
                    return await response.Content.ReadAsStringAsync();
                }
            }
        }

        private async Task DownloadResourcesAsync(IHtmlDocument document, string path)
        {
            // filter resources: pictures, pdf files, etc.
            var resources = from element in document.All
                from attribute in element.Attributes
                where fileExtensions.Any(e => attribute.Value.EndsWith(e) && attribute.Value.StartsWith("http"))
                select attribute;

            using (var client = new HttpClient())
            {
                // save resources
                foreach (var item in resources)
                {
                    // get
                    using (var response = await client.GetAsync(item.Value))
                    {
                        // read
                        using (var stream = await response.Content.ReadAsStreamAsync())
                        {
                            // save
                            using (var streamWrite = File.Open(path + @"\" + GetFilename(item.Value), FileMode.Create))
                            {
                                await stream.CopyToAsync(streamWrite);
                            }
                        }
                    }
                }
            }
        }

        private string GetFilename(string hreflLink)
        {
            return Path.GetFileName(hreflLink);
        }

        /// <summary>
        /// Html parser
        /// </summary>
        /// <param name="html">html as string</param>
        /// <returns></returns>
        private async Task<IHtmlDocument> ParseHtmlAsync(string html)
        {
            var parser = new AngleSharp.Parser.Html.HtmlParser();
            var document = await parser.ParseAsync(html);
            return document;
        }

        // Saves html local
        private async Task SaveHtmlAsync(string path, string htmlDocument)
        {
            using (var outputFile = new StreamWriter(Path.Combine(path, "Html.htm")))
            {
                await outputFile.WriteAsync(htmlDocument);
            }
        }

        private async Task StartSavingAsync(int loadLevel, List<IElement> list, string startPath)
        {
            var path = startPath;

            if (loadLevel > 0)
            {
                Console.WriteLine($"Processing {url}" + '\n' + "Downloading html");
                var htmlDoc = await DownloadHtmlAsync(url);

                var parsedHtml = await ParseHtmlAsync(htmlDoc);

                await SaveHtmlAsync(path, htmlDoc); // save html

                Console.WriteLine("Html downloaded");

                var resourcePath = CreateFolder(path, "resources");

                Console.WriteLine("Created folder: resources" + '\n' + "Downloading resources");
                await DownloadResourcesAsync(parsedHtml, resourcePath); // download resources

                Console.WriteLine("All resources are downloaded");

                path = CreateFolder(path, "links"); // create links folder
                Console.WriteLine("Created folder: links" + '\n');

                var elements = parsedHtml.QuerySelectorAll("a").Where(c =>
                    !string.IsNullOrEmpty(c.GetAttribute("href")) && c.GetAttribute("href").Contains(url)).ToList();

                foreach (var b in elements.Take(3)) // for 3 links recursievely call method
                {
                    var subPath = path;
                    url = b.GetAttribute("href");
                    subPath = CreateFolder(path, Path.GetRandomFileName());
                    await StartSavingAsync(loadLevel - 1, elements, subPath);
                }

                Console.WriteLine("Work is done!");
            }
        }
    }
}
